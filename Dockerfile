FROM openjdk:17.0.2-oraclelinux8
MAINTAINER haipham
COPY target/service02-0.0.1-SNAPSHOT.jar service02.jar
ENTRYPOINT [ "java", "-Dspring.profiles.active=prod", "-jar", "/service02.jar"]
