package com.service02.service

import com.service02.model.CovidCountry
import com.service02.repository.CovidCountryRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class CovidCountryServiceTest {
    private val covidCountryRepo = mockk<CovidCountryRepository>(relaxed = true)
    private val covidCountryService = CovidCountryService(covidCountryRepo)

    private val c1 = CovidCountry("1", "Vietnam", "Asia", 10000, 1000, 9000, "https://disease.sh/")

    @AfterEach
    fun teardown(){
        unmockkAll()
    }

    @Test
    fun `add new record to elasticsearch`() {
        every { covidCountryRepo.findByCountryAndSource(allAny(), allAny()) } returns null
        every { covidCountryRepo.save(allAny()) } returns c1

        covidCountryService.saveCovidCountry(c1)

        verify { covidCountryRepo.findByCountryAndSource(allAny(), allAny()) }
        verify { covidCountryRepo.save(allAny()) }
    }

    @Test
    fun `update record to elasticsearch`(){
        every { covidCountryRepo.findByCountryAndSource(allAny(), allAny()) } returns c1
        every { covidCountryRepo.save(allAny()) } returns c1

        covidCountryService.saveCovidCountry(c1)

        verify { covidCountryRepo.findByCountryAndSource(allAny(), allAny()) }
        verify { covidCountryRepo.delete(c1) }
        verify { covidCountryRepo.save(c1) }
    }
}