package com.service02.model

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.Document

@Document(indexName = "covidcountry")
data class CovidCountry (
    @Id
    @JsonProperty("id")
    var id: String? = null,
    @JsonProperty("country")
    var country: String?,
    @JsonProperty("continent")
    var continent: String?,
    @JsonProperty("cases")
    var cases: Int?,
    @JsonProperty("deaths")
    var deaths: Int?,
    @JsonProperty("recovered")
    var recovered: Int?,
    @JsonProperty("source")
    var source: String?
){}