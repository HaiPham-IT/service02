package com.service02

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Service02Application

fun main(args: Array<String>) {
	runApplication<Service02Application>(*args)
}
