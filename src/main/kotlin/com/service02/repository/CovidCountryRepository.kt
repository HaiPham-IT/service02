package com.service02.repository

import com.service02.model.CovidCountry
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository
import org.springframework.stereotype.Repository

@Repository
interface CovidCountryRepository: ElasticsearchRepository<CovidCountry, String> {
    fun findByCountryAndSource(country: String?, source: String?): CovidCountry?
}