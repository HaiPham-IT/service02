package com.service02.service

import com.service02.model.CovidCountry
import mu.KotlinLogging
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class KafkaConsumerService(private var covidCountryService: CovidCountryService) {
    private var log = KotlinLogging.logger {  }

    @KafkaListener(
        topics = ["\${spring.kafka.consumer.topic}"],
        groupId = "\${spring.kafka.consumer.group}",
				containerFactory = "covidCountryKafkaListenerContainerFactory"
    )
    fun listen1(record: CovidCountry){
        log.info { "Listen1 Consumed ${record.toString()}" }
        handleCovidCountryRecord(record)
    }

    @KafkaListener(
        topics = ["\${spring.kafka.consumer.topic}"],
        groupId = "\${spring.kafka.consumer.group}",
				containerFactory = "covidCountryKafkaListenerContainerFactory"
    )
    fun listen2(record: CovidCountry){
        log.info { "Listen2 Consumed ${record.toString()}" }
        handleCovidCountryRecord(record)
    }

    fun handleCovidCountryRecord(record: CovidCountry){
        try {
            covidCountryService.saveCovidCountry(record)
            log.info { "Push to Elasticsearch: $record" }
        }
        catch (e: Exception){
            log.error { "Error: $e" }
        }

    }
}