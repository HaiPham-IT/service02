package com.service02.service

import com.service02.model.CovidCountry
import com.service02.repository.CovidCountryRepository
import mu.KotlinLogging
import org.springframework.stereotype.Service

@Service
class CovidCountryService(private var covidCountryRepo: CovidCountryRepository) {
    private var log = KotlinLogging.logger {  }

    fun saveCovidCountry(covidCountry: CovidCountry){
        try {
            when(val record = covidCountryRepo.findByCountryAndSource(covidCountry.country, covidCountry.source)){
                null -> {
                    covidCountryRepo.save(covidCountry)
                    log.info { "Save record: $covidCountry" }
                }
                else -> {
                    covidCountryRepo.delete(record)
                    covidCountryRepo.save(covidCountry)
                    log.info { "Update record: $record" }
                }
            }
        }
        catch (e: Exception){
            log.error { "Error while push data to Elasticsearch: $e" }
        }


    }
}