package com.service02.kafka

import com.service02.model.CovidCountry
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.support.serializer.JsonDeserializer

@Configuration
class KafkaConsumerConfig(
    @Value("\${spring.kafka.bootstrap-server}")
    private var bootstrapServer: String,
    @Value("\${spring.kafka.consumer.group}")
    private var group: String
) {
    @Bean
    fun covidCountryConsumerFactory(): ConsumerFactory<String, CovidCountry> {
        val config = mapOf<String, Any>(
            ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServer,
            ConsumerConfig.GROUP_ID_CONFIG to group,
            ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG to StringDeserializer::class.java,
            ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG to JsonDeserializer::class.java,
            JsonDeserializer.USE_TYPE_INFO_HEADERS to false
        )
        return DefaultKafkaConsumerFactory(config, StringDeserializer(), JsonDeserializer(CovidCountry::class.java))
    }

    @Bean
    fun covidCountryKafkaListenerContainerFactory(): ConcurrentKafkaListenerContainerFactory<String, CovidCountry>{
        val factory = ConcurrentKafkaListenerContainerFactory<String, CovidCountry>()
        factory.consumerFactory = covidCountryConsumerFactory()
        return factory
    }
}